## docsify-cli

This image will allow you to run docsify templates locally.

### Usage
```shell
docker run -d --rm -p 3000:3000 -v $PATH_TO_docs/index.html:/tmp/docsify/ registry.gitlab.com/avessche/docsify-cli:latest
```
Browse to http://localhost:3000